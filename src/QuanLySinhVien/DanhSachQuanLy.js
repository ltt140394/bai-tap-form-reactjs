import React, { Component } from "react";
import { connect } from "react-redux";
import { actionCreator } from "./redux/constants/action";

class DanhSachQuanLy extends Component {
  state = {
    searchList: [],
  };

  render() {
    const handleSearch = (array) => {
      let result = array.filter((sinhVien, index) => {
        if (document.getElementById("selectSearch").value == "maSV") {
          return (
            sinhVien.maSV.toUpperCase() ==
            document.getElementById("txtSearch").value.toUpperCase()
          );
        } else if (document.getElementById("selectSearch").value == "hoTen") {
          return (
            sinhVien.hoTen.toUpperCase() ==
            document.getElementById("txtSearch").value.toUpperCase()
          );
        } else if (document.getElementById("selectSearch").value == "soDT") {
          return (
            sinhVien.soDienThoai == document.getElementById("txtSearch").value
          );
        } else {
          return (
            sinhVien.email.toUpperCase() ==
            document.getElementById("txtSearch").value.toUpperCase()
          );
        }
      });

      if (result.length == 0) {
        document.getElementById("pThongBao").innerText =
          "Không tìm thấy dữ liệu";
      } else {
        document.getElementById("pThongBao").innerText = "";
        this.setState({ searchList: result });
      }
    };

    return (
      <div className="py-5">
        <div className="txtSearch text-right position-relative">
          <input
            type="text"
            className="w-75 p-2"
            id="txtSearch"
            placeholder="Search..."
          />
          <img
            src="./img/pngfind.com-search-icon-png-1017087.png"
            alt=""
            style={{
              width: "20px",
              height: "20px",
              position: "absolute",
              top: "50%",
              right: "10px",
              transform: "translateY(-50%)",
              cursor: "pointer",
            }}
            onClick={() => {
              handleSearch(this.props.danhSachSinhVien);
            }}
          />
          <select
            class="form-control w-25"
            id="selectSearch"
            style={{
              position: "absolute",
              top: "50%",
              right: "40px",
              transform: "translateY(-50%)",
              border: "none",
            }}
          >
            <optgroup label="Tìm kiếm theo :"></optgroup>
            <option value="maSV">Mã số SV</option>
            <option value="hoTen">Họ tên</option>
            <option value="soDT">Số điện thoại</option>
            <option value="email">Email</option>
          </select>
        </div>
        <p className="text-center text-danger mb-4" id="pThongBao"></p>
        <table className="table text-left">
          <thead className="bg-dark text-white font-weight-bold p-3">
            <tr>
              <td>Mã SV</td>
              <td>Họ tên</td>
              <td>Số điện thoại</td>
              <td>Email</td>
              <td>Thao tác</td>
            </tr>
          </thead>
          <tbody>
            {(this.state.searchList.length == 0
              ? this.props.danhSachSinhVien
              : this.state.searchList
            ).map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.maSV}</td>
                  <td>{item.hoTen}</td>
                  <td>{item.soDienThoai}</td>
                  <td>{item.email}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleXoaSinhVien(item.taiKhoan);
                      }}
                      className="btn btn-warning"
                    >
                      Xóa
                    </button>
                    <button
                      onClick={() => {
                        this.props.handleSuaSinhVien(item);
                      }}
                      className="btn btn-primary mx-2"
                    >
                      Sửa
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    danhSachSinhVien: state.sinhVienReducer.danhSachSinhVien,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleXoaSinhVien: (taiKhoan) => {
      dispatch(actionCreator.xoaSinhVien(taiKhoan));
    },

    handleSuaSinhVien: (sinhVien) => {
      dispatch(actionCreator.suaSinhVien(sinhVien));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DanhSachQuanLy);
