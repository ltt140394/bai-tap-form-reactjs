import React, { Component } from "react";
import { connect } from "react-redux";
import { actionCreator } from "./redux/constants/action";

class FormQuanLy extends Component {
  state = {
    infor: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      hoTen: "",
    },
  };

  handleChangeForm = (event) => {
    let value = event.target.value;
    let name = event.target.name;

    this.setState({ infor: { ...this.state.infor, [name]: value } }, () => {
      // console.log(this.state.infor);
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    if (this.props.sinhVienDaSua) {
      this.props.handleCapNhatSinhVien([
        this.state.infor.taiKhoan,
        this.state.infor,
      ]);
    } else {
      this.props.handleThemSinhVien(this.state.infor);
      this.setState({
        infor: {
          maSV: "",
          soDienThoai: "",
          email: "",
          hoTen: "",
        },
      });
    }
  };

  UNSAFE_componentWillReceiveProps = (newProps) => {
    if (newProps.sinhVienDaSua) {
      this.setState({ infor: newProps.sinhVienDaSua });
      document.getElementById("maSV").setAttribute("readonly", "");
    } else {
      document.getElementById("maSV").removeAttribute("readonly");
      this.setState({
        infor: {
          maSV: "",
          soDienThoai: "",
          email: "",
          hoTen: "",
        },
      });
    }
  };

  render() {
    return (
      <div>
        <form
          onSubmit={(e) => {
            this.handleSubmit(e);
          }}
        >
          <div className="row">
            <div className="form-group col-6">
              <label htmlFor="" style={{ fontSize: "20px" }}>
                Mã SV
              </label>
              <input
                type="text"
                onChange={(event) => {
                  this.handleChangeForm(event);
                }}
                className="form-control w-100"
                value={this.state.infor.maSV}
                name="maSV"
                id="maSV"
                placeholder="Nhập mã sinh viên"
              />
            </div>
            <div className="form-group col-6">
              <label htmlFor="" style={{ fontSize: "20px" }}>
                Họ tên
              </label>
              <input
                type="text"
                onChange={(event) => {
                  this.handleChangeForm(event);
                }}
                className="form-control w-100"
                value={this.state.infor.hoTen}
                name="hoTen"
                id=""
                placeholder="Nhập họ tên"
              />
            </div>
            <div className="form-group col-6">
              <label htmlFor="" style={{ fontSize: "20px" }}>
                Số điện thoại
              </label>
              <input
                type="text"
                onChange={(event) => {
                  this.handleChangeForm(event);
                }}
                className="form-control w-100"
                value={this.state.infor.soDienThoai}
                name="soDienThoai"
                id=""
                placeholder="Nhập số điện thoại"
              />
            </div>
            <div className="form-group col-6">
              <label htmlFor="" style={{ fontSize: "20px" }}>
                Email
              </label>
              <input
                type="text"
                onChange={(event) => {
                  this.handleChangeForm(event);
                }}
                className="form-control w-100"
                value={this.state.infor.email}
                name="email"
                id=""
                placeholder="Nhập email"
              />
            </div>
          </div>
          <button type="submit" className="btn btn-success mt-2">
            {this.props.sinhVienDaSua ? "Cập nhật" : "Thêm sinh viên"}
          </button>
        </form>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    sinhVienDaSua: state.sinhVienReducer.sinhVienDaSua,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleThemSinhVien: (sinhVien) => {
      dispatch(actionCreator.themSinhVien(sinhVien));
    },

    handleCapNhatSinhVien: ([taiKhoan, sinhVienCanSua]) => {
      dispatch(actionCreator.capNhatSinhVien([taiKhoan, sinhVienCanSua]));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormQuanLy);
