import React, { Component } from "react";
import { connect } from "react-redux";
import DanhSachQuanLy from "./DanhSachQuanLy";
import FormQuanLy from "./FormQuanLy";

class QuanLySinhVien extends Component {
  render() {
    return (
      <div className="container text-left">
        <h2 className="bg-dark text-white p-3 my-4">Thông tin sinh viên</h2>
        <FormQuanLy />
        {this.props.danhSachSinhVien.length > 0 && <DanhSachQuanLy />}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    danhSachSinhVien: state.sinhVienReducer.danhSachSinhVien,
  };
};

export default connect(mapStateToProps)(QuanLySinhVien);
