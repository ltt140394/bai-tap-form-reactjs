import { ADD_SV, EDIT_SV, REMOVE_SV, UPDATE_SV } from "./constants";

export const actionCreator = {
  themSinhVien: (index) => {
    return {
      type: ADD_SV,
      payload: index,
    };
  },

  xoaSinhVien: (index) => {
    return {
      type: REMOVE_SV,
      payload: index,
    };
  },

  suaSinhVien: (index) => {
    return {
      type: EDIT_SV,
      payload: index,
    };
  },

  capNhatSinhVien: (index) => {
    return {
      type: UPDATE_SV,
      payload: index,
    };
  },
};
