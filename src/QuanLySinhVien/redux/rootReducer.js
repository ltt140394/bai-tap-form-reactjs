import { combineReducers } from "redux";
import { sinhVienReducer } from "./sinhVienReducer";

export const rootReducer = combineReducers({
  sinhVienReducer,
});
