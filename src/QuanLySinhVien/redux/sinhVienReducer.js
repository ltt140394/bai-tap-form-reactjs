import { ADD_SV, EDIT_SV, REMOVE_SV, UPDATE_SV } from "./constants/constants";

let initialState = {
  danhSachSinhVien: [],
  sinhVienDaSua: null,
};

export const sinhVienReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_SV: {
      let cloneDSSV = [...state.danhSachSinhVien];
      cloneDSSV.push(payload);
      state.danhSachSinhVien = cloneDSSV;
      return { ...state };
    }

    case REMOVE_SV: {
      let cloneDSSV = [...state.danhSachSinhVien];

      let index = cloneDSSV.findIndex((item) => {
        return item.taiKhoan == payload;
      });

      cloneDSSV.splice(index, 1);
      state.danhSachSinhVien = cloneDSSV;
      return { ...state };
    }

    case EDIT_SV: {
      state.sinhVienDaSua = payload;
      return { ...state };
    }

    case UPDATE_SV: {
      let cloneDSSV = [...state.danhSachSinhVien];

      let index = cloneDSSV.findIndex((item) => {
        return item.taiKhoan == payload[0];
      });

      cloneDSSV[index] = payload[1];
      state.danhSachSinhVien = cloneDSSV;
      state.sinhVienDaSua = null;
      return { ...state };
    }
    default:
      return state;
  }
};
